<!DOCTYPE html>
<html lang="en">
<head>
  <title> Live Search</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
</head>
<body>
<?php 
 include("ex3_connection.php");
?>
<div class="container mt-4">
    <p><h2>Live Search</h2></p>
    <h6 class="mt-5"><b>Search Name</b></h6>
    <div class="input-group mb-4 mt-3">
         <div class="form-outline">
            <input type="text" id="getName"/>
        </div>
    </div>                   
    <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
          </tr>
        </thead>
        <tbody id="userTableBody">

        <?php
         $sql = "SELECT * FROM users";
         $query = mysqli_query($con, $sql);
         while ($row = mysqli_fetch_assoc($query)) {
            echo"<tr>";
            echo"<td>".$row['id']."</td>";
            echo"<td>".$row['name']."</td>";
            echo"<td>".$row['email']."</td>";
            echo"<td>".$row['phone']."</td>";
            echo"</tr>";
         }
        ?>
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function(){
     $('#getName').on("keyup", function(){
        var getName =$(this).val();
        $.ajax({
            method:'POST',
            url:'searchajax.php',
            data:{name:getName},
            success:function(response){
              $("#userTableBody").html(response);
            }
        });
     });
});
</script>
          
</body>
</html>
